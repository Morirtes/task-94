import EventEmitter from "eventemitter3";

export default class Application extends EventEmitter {
  static get events() {
    return {
      READY: "ready",
    };
  }

  constructor() {
    super();
    this.emojis = [];
    this.banana = "🍌";
    this.emit(Application.events.READY);
  }
  setEmojis(emojis) {
    this.emojis = emojis;
  }

  addBananas() {
    const emojis = this.emojis.map((monkey) => monkey + this.banana);
    this.emojis = emojis;
    // console.log(emojis);
    const para = document.createElement("p");
    para.innerHTML = emojis;
    document.getElementById("emojis").appendChild(para);
  }
}
